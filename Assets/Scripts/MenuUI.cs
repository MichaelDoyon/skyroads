﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUI : MonoBehaviour
{
    public void OnLevelButtonClick(int levelIndex)
    {
        GameManager.Instance.TryLoadLevel(levelIndex);
    }
}
