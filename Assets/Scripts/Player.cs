﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // CONST
    private const float DISTANCE_GROUNDED = 0.4f;
    private const float INNER_OFFSET_GROUNDED = 0.05f;

    public GameObject deathParticles;

    private Vector3 moveVector;
    private float verticalVelocity;
    private CharacterController controller;
    private float gravity = 21.0f;
    private float jumpForce = 7.0f;
    private CollisionFlags flags;
    private bool isAlive = true;

    // Fuel / throttle
    private float throttle = 0;
    private float sideSpeed = 3.0f;
    private float maxSpeed = 7.5f;
    private float acceleration = 0.1f;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (!isAlive)
            return;

        Vector3 inputs = Vector3.zero;
        inputs.x = Input.GetAxisRaw("Horizontal");
        inputs.z = Input.GetAxis("Vertical");
        inputs.y = (Input.GetKeyDown(KeyCode.Space)) ? 1 : 0;

        if (Grounded())
        {
            moveVector.x = inputs.x * sideSpeed;
            verticalVelocity = 0;
            if (inputs.y == 1) // Jump input was pressed
            {
                verticalVelocity = jumpForce;
            }
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }
        moveVector.y = verticalVelocity;

        throttle += inputs.z * acceleration;
        throttle = Mathf.Clamp(throttle, 0, maxSpeed);
        moveVector.z = throttle;

        flags = controller.Move(moveVector * Time.deltaTime);

        // DeathCheck
        if (transform.position.y <= -2.0f)
            Death();
    }

    public virtual bool Grounded()
    {
        float yRay = controller.bounds.center.y - (controller.height * 0.5f) + 0.3f;
        RaycastHit hit;

        // Mid
        if (Physics.Raycast(new Vector3(controller.bounds.center.x, yRay, controller.bounds.center.z), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            return true;
        }

        // Right
        if (Physics.Raycast(new Vector3(controller.bounds.center.x + (controller.bounds.extents.x - INNER_OFFSET_GROUNDED), yRay, controller.bounds.center.z), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            return true;
        }

        // Front-Left
        if (Physics.Raycast(new Vector3(controller.bounds.center.x - (controller.bounds.extents.x - INNER_OFFSET_GROUNDED), yRay, controller.bounds.center.z), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            return true;
        }

        return false;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (flags == CollisionFlags.Sides)
        {
            Death();
        }
    }

    private void Death()
    {
        // Cast death particles
        isAlive = false;
        Instantiate(deathParticles, transform);
        transform.GetChild(0).gameObject.SetActive(false);
        Invoke("ToMenu", 2.0f);
    }

    private void ToMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
