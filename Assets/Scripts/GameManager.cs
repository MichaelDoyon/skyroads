﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
    private byte[] levelProgress;

    public int LevelToLoad { set; get; }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        SceneManager.LoadScene("Menu");
    }
    public void TryLoadLevel(int level)
    {
        // Look if its unlocked

        LevelToLoad = level;
        SceneManager.LoadScene("Game");
    }
}
