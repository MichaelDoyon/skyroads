﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform lookAt; // Player most likely
    public Vector3 offset;

    private void LateUpdate()
    {
        transform.position = new Vector3(offset.x, offset.y, offset.z + lookAt.position.z);
    }
}
