﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    private void Start()
    {
        int levelIndex = GameManager.Instance.LevelToLoad;
        SceneManager.LoadScene(levelIndex.ToString(), LoadSceneMode.Additive);
    }
}
